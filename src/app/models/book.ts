export type Book = {
    id: number
    isbn: string
    title: string
    author: string
    coverImg: string
    blurb: string,
    notes:string
  }