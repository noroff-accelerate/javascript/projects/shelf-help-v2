import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { NotesComponent } from './components/notes/notes.component';
import { BookListItemComponent } from './components/book-list-item/book-list-item.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ShelfPageComponent } from './pages/shelf-page/shelf-page.component';
import { NotesPageComponent } from './pages/notes-page/notes-page.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    BookListComponent,
    NotesComponent,
    BookListItemComponent,
    LoginPageComponent,
    ShelfPageComponent,
    NotesPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
