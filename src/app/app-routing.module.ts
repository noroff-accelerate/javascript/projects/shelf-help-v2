import { NgModule } from "@angular/core";
import { RouterModule,Routes } from "@angular/router";
import { LoginPageComponent } from "./pages/login-page/login-page.component";
import { ShelfPageComponent } from "./pages/shelf-page/shelf-page.component";
import { NotesPageComponent } from "./pages/notes-page/notes-page.component";
import { AuthGuard } from "./guards/auth.guard";
import { LogingFormGuard } from "./guards/loging-form.guard";


const routes: Routes = [
    {
        path:'',
        pathMatch:"full",
        component:LoginPageComponent,
        canActivate:[LogingFormGuard]
    },
    {
        path:'shelf',
        component:ShelfPageComponent,
        canActivate:[AuthGuard]
    },
    {
        path:'notes/:bookId',
        component:NotesPageComponent,
        canActivate:[AuthGuard]
    }
]

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule{

}