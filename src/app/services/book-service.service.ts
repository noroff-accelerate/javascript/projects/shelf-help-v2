import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from '../models/book';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private readonly http:HttpClient) { }

  getBooks():Observable<Book[]>{
    return this.http.get<Book[]>('http://localhost:3000/books')
  }

  getBookById(id:number):Observable<Book>{
    return this.http.get<Book>(`http://localhost:3000/books/${id}`)
  }

  updateBook(book:Book){
    return this.http.put(`http://localhost:3000/books/${book.id}`,book)
  }
}
