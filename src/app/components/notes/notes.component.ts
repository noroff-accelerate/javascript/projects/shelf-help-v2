import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { error } from 'console';
import { Book } from 'src/app/models/book';
import { User } from 'src/app/models/user';
import { BookService } from 'src/app/services/book-service.service';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  public currentBook?: Book

  constructor(private readonly route: ActivatedRoute, private readonly bookService: BookService, private readonly userService:UserService) {
  }



  ngOnInit(): void {
    this.bookService.getBookById(Number(this.route.snapshot.paramMap.get('bookId')))
      .subscribe({
        next: book => this.currentBook = book,
        error: (error => console.log(error))
      })

  }

  updateNotes(notesForm:NgForm){

      let updatedBook:Book =  {
        ...this.currentBook,
        notes:notesForm.value.notes
      } as Book

      this.currentBook = updatedBook

      this.bookService.updateBook(updatedBook)
      .subscribe({
        next:response => console.log(response),
        error:error => console.log(error)
      })

  }


  get user(): User{
    return this.userService.user
  }


}
