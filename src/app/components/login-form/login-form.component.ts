import { Component, OnDestroy } from "@angular/core";
import { NgForm, NgModel } from "@angular/forms";
import { Router } from "@angular/router";
import { finalize, map, tap } from "rxjs";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user-service.service";

@Component({
    selector:"app-login-form",
    templateUrl: "./login-form.component.html",
    styleUrls:["./login-form.component.css"]
}


)
export class LoginFormComponent implements OnDestroy{
    private _user!: User

    constructor(private readonly router:Router, private readonly userService:UserService){}

    ngOnDestroy(): void {
        this.userService.user = this._user
        this.userService.postUser(this._user)
    }
    
    login(form:NgForm){

       this.userService.loginAttempt()
       .pipe(
        map(users => users.filter(user => user.username == form.value.username)),
    )
    .subscribe({
        next: validUsers => {
            if (validUsers.length == 0) {
                // not found
                let newUser: User = {
                    id: 0,
                    username: form.value.username,
                    fullname: "unknown"
                }
                this._user = newUser
                localStorage.setItem('username', newUser.username!)
                this.router.navigateByUrl('shelf')
            }
            else {
                // found
                this._user = validUsers[0]
                localStorage.setItem('username', validUsers[0].username!)
                this.router.navigateByUrl('shelf')
            }
        },
        error: error => {
            console.log(error)
        }
    })
      
       

       

    }

 

}