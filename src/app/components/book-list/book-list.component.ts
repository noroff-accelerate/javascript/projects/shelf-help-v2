import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { User } from 'src/app/models/user';
import { BookService } from 'src/app/services/book-service.service';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  public books: Book[] = []

  constructor(private readonly router: Router, private readonly bookService: BookService, private readonly userService: UserService) { }

  ngOnInit(): void {
    this.bookService.getBooks()
      .subscribe({
        next: (books) => {
          this.books = books
        },
        error: (error => {
          console.log(error)
        })
      }

      )
  }

  logout() {
    localStorage.clear()
    this.router.navigateByUrl('')
  }


  get user(): User {
    return this.userService.user
  }

  handleBookSelected(bookId: number) {
    this.router.navigate(['notes', bookId])
  }



}
